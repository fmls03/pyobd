import obd

# si connette automaticamente alla porta usb o rf oppure specifica la porta
connection = obd.OBD() 

# esegue un comando OBD qualsiasi su un sensore
comando = obd.commands.SPEED # solo di esempio


# invia il comando e preleva la risposta
risposta = connection.query(comando)


# Printa i valori unitari
print(risposta.value)

